package API_Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

//{"request": {
//        "method": "GET",
//        "url": "/akhodnev/test"
//        },
//        "response": {
//        "status": 200,
//        "headers": {
//        "Content-Type": "application/json"
//        },
//        "jsonBody": {
//        "postcheck": "success",
//        "name": "Anastasiia",
//        "lastname": "Khodneva"
//        }
//        }


public class APItest {

    RequestSpecification reguestSpecBuilder;
    public static Map<Object,Object> map = new HashMap<Object,Object>();

    @BeforeMethod
    public void init(){
        reguestSpecBuilder = given().baseUri("http://wiremock.atlantis.t-systems.ru/").
                filter(new RequestLoggingFilter()).
                filter(new ResponseLoggingFilter());
//        Map<String,String> map1 = new HashMap<String,String>();
//        map1.put("method","GET");
//        map1.put("url","/agroshev/test01");
//        map.put("request","map1");
//        Map<String,String> map2 = new HashMap<String,String>();
//        map2.put("status","200");
//        map.put("response","map2");
    }

    @Test
    public void test(){

        given().spec(reguestSpecBuilder).
                when()
                .get("/agroshev/test01")
                .then().assertThat().statusCode(200)
                .body("$", Matchers.hasEntry("name","Kitty"))
                .header("Content-Type","application/json"); // application/json
    }
// same test but from "request" Tag
    @Test
    public void test_1(){

        given().spec(reguestSpecBuilder).
                when()
                .request("GET","/agroshev/test01")
                .then().assertThat().statusCode(200)
//                .body("$", Matchers.hasEntry("name","Kitty"))
                .header("Content-Type","application/json");  //application/json
    }

    // update mapping(reguest/response) with id 6379b911-6da5-4cc8-8674-64b4ea050ef0
    @Test
    public void updateMappingTest(){

        given().spec(reguestSpecBuilder)
//                .body(map)
                .when()
                .request("GET","/__admin/mappings/6379b911-6da5-4cc8-8674-64b4ea050ef0")
                .then().assertThat().statusCode(200);
    }

    // my API:
//    {
//        "request": {
//        "method": "GET",
//                "url": "/agroshev/test01"
//    },
//        "response": {
//        "status": 200,
//                "jsonBody": {
//            "anthem": "Hello Cat!",
//                    "name": "Kitty",
//                    "lastname": "Cat"
//        },
//        "headers": {
//            "Content-Type": "text/plain"
//        }
//    }
//    }


}
