package API_Test;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThan;

public class APIpractice01 {

    RequestSpecification reguestSpecBuilder;
    ResponseSpecification responseSpecBuilder;


    @BeforeMethod
    public void init(){
//        RestAssured.baseURI = "http://wiremock.atlantis.t-systems.ru/";
//        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

        reguestSpecBuilder = given().baseUri("http://wiremock.atlantis.t-systems.ru/").
                filter(new RequestLoggingFilter()).
                filter(new ResponseLoggingFilter()); //.
//                filter(new NewOneCustomTestFilter());  add this filter if you have some specific Ending of path for this TEst(ex.: /agroshev/test01/special_ending)

        responseSpecBuilder = expect().
                statusCode(200).
                contentType("application/json").
                header("Content-Type","application/json").
                body("$", Matchers.hasEntry("name","Kitty"));
    }

    @Test
    public void measureResponseTestOld(){

//        Response response = RestAssured.
//                when().get("/agroshev/test01");
//
//        long timeInMiliseconds = response.time();
//        long timeInSeconds = response.timeIn(TimeUnit.SECONDS);
//        System.out.println(timeInMiliseconds);
//        System.out.println(timeInSeconds);

        given().spec(reguestSpecBuilder).
                expect().
                spec(responseSpecBuilder).
                when().
                get("/agroshev/test01").
                then().time(lessThan(5000L)).
                assertThat().body("size()", Matchers.is(3));

//        RestAssured.when()
//                .get("/agroshev/test01")
//                .then().time(lessThan(5000L));
    }


    @Test
    public void measureResponseTest(){

        Response response = RestAssured.
                when().get("/agroshev/test01");

        long timeInMiliseconds = response.time();
        long timeInSeconds = response.timeIn(TimeUnit.SECONDS);
        System.out.println(timeInMiliseconds);
        System.out.println(timeInSeconds);
        RestAssured.when()
                .get("/agroshev/test01")
                .then().time(lessThan(5000L));
    }

    @Test
    public void measureResponseTest_01(){

        Response response = RestAssured.
                when().get("/agroshev/test01");

        long timeInMiliseconds = response.time();
        long timeInSeconds = response.timeIn(TimeUnit.SECONDS);
        System.out.println(timeInMiliseconds);
        System.out.println(timeInSeconds);
        RestAssured.when()
                .request("GET","/agroshev/test01")
                .then().time(lessThan(5000L));
    }
}
